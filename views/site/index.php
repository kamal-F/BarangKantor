<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Biasa Barang Kantor!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">


        <div class="row">
            <div class="col-lg-4">
                <h2>setup barangkantor.bom</h2>
                <p>Setup apache</p>
                <p>/opt/lampp/etc/extra/httpd-vhosts.conf</p>
				<p>
				<code>
				<?php 
				echo Html::encode('
						<VirtualHost *:80>
				    ServerAdmin barangkantor.bom
				    DocumentRoot "/opt/lampp/htdocs/biasa/web"
				    ServerName  barangkantor.bom
					<Directory "/opt/lampp/htdocs/biasa/web">
					   Options Indexes MultiViews FollowSymLinks
					   AllowOverride All
					   Require all granted	   	   
					</Directory>   	
				</VirtualHost>
						');
				?>
				</code>
				</p>
				
                
				<p>/etc/hosts
				127.0.0.1	barangkantor.bom
				</p>
				<p>
				<code>
				127.0.0.1	barangkantor.bom
				</code>
				</p>
				<p>restart apache</p>
				<p>--------------</p>
				<p>Setup nginx</p>
				<p>
				<p>/etc/nginx/sites-enabled/barangkantor.bom</p>
				<code>
				<?php 
				echo Html::encode('
				# Virtual Host configuration for example.com
#
# You can move that to a different file under sites-available/ and symlink that
# to sites-enabled/ to enable it.
#
server {
	listen 80;
	listen [::]:80;

	server_name barangkantor.bom;

	root /var/www/html/barangkantor/web;
	index index.php;

	location / {
		#try_files $uri $uri/ =404;
		try_files $uri $uri/ /index.php?$args;
	}
	# pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
	#
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
	#
	#	# With php7.0-cgi alone:
		fastcgi_pass 127.0.0.1:9000;
	#	# With php7.0-fpm:
	#	fastcgi_pass unix:/run/php/php7.2-fpm.sock;
	}

	# deny access to .htaccess files, if Apaches document root
	# concurs with nginx s one
	#
	#location ~ /\.ht {
	#	deny all;
	#}
}
');
				?>
				</code>
				</p>
				<p>/etc/hosts
				127.0.0.1	barangkantor.bom
				</p>
				<p>
				<code>
				127.0.0.1	barangkantor.bom
				</code>
				</p>
				<p>restart nginx</p>
				
                <p><a class="btn btn-default" href="#">Beranda &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Web Service Restful</h2>

                <p>Fitur Proyek 2 Restful web service</p>

                <p><a class="btn btn-default" href="<?= Yii::$app->urlManager->createUrl(['site/about']) ?>">about Ws &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Daftar</h2>

                <p>Pada basic ditambahkan fitur sign up mengadopsi dari advance template.</p>

                <p><a class="btn btn-default" href="<?= Yii::$app->urlManager->createUrl(['site/signup']) ?>">Basic Sign Up&raquo;</a></p>
            </div>
        </div>

    </div>
</div>
