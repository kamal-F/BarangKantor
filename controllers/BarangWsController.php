<?php
/*
token diberikan jika admin, rule saya sendiri

Pass custom header, no user name: pass/ token
//all xml
curl -i -H "Accept:application/xml" "http://localhost/biasa/web/barang-ws"
//all json
curl -i -H "Accept:application/json" "http://localhost/biasa/web/barang-ws"
//show id = 24
curl -i -H "Accept:application/xml" -XGET "http://localhost/biasa/web/barang-ws/24"
//create baru
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://localhost/biasa/web/barang-ws" -d '{"kode": "x2", "nama": "termos","deskripsi":"barang bagus","id_kantor":"10"}'
//delete id = 26
curl -i -H "Accept:application/xml" -XDELETE "http://localhost/biasa/web/barang-ws/26"
//update id = 27
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPUT "http://localhost/biasa/web/barang-ws/27" -d '{"nama": "termos ubahan","deskripsi":"barang bagus"}'
--------------------------
use basic
curl -u bb: "http://localhost/biasa/web/barang-ws"
curl -u bb: "Accept:application/json" "http://localhost/biasa/web/barang-ws"
curl -u bb: "Accept:application/xml" -XGET "http://localhost/biasa/web/barang-ws/24"
curl -u bb: "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://localhost/biasa/web/barang-ws" -d '{"kode": "B25", "nama": "reskuker","deskripsi":"Penanak nasi bagus","id_kantor":"10"}'
curl -u bb: "Accept:application/xml" -XDELETE "http://localhost/biasa/web/barang-ws/26"
curl -u bb: "Accept:application/json" -H "Content-Type:application/json" -XPUT "http://localhost/biasa/web/barang-ws/27" -d '{"nama": "termos ubahan","deskripsi":"barang bagus dan awet"}'
curl -u bb: "http://localhost/biasa/web/barang-ws/lihat?nama=ter"
----------

Port number ended with 'a'
use basic rev to barangkantor.bom
curl -u bb:123456 "http://barangkantor.bom/barang-ws"
curl -u bb:123456 "Accept:application/json" "http://barangkantor.bom/barang-ws"
curl -u bb:123456 "Accept:application/xml" -XGET "http://barangkantor.bom/barang-ws/24"
curl -u bb:123456 "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://barangkantor.bom/barang-ws" -d '{"kode": "B25a", "nama": "ember","deskripsi":"untuk tampung air","id_kantor":"10"}'
curl -u bb:123456 "Accept:application/xml" -XDELETE "http://barangkantor.bom/barang-ws/26"
curl -u bb:123456 "Accept:application/json" -H "Content-Type:application/json" -XPUT "http://barangkantor.bom/barang-ws/27" -d '{"nama": "termos aseli","deskripsi":"termos aseli li"}'
curl  "http://barangkantor.bom/barang-ws/lihat?nama=ter"

atau tanpa "Accept:application/json"
curl -u bb:123456 -H "Content-Type:application/json" -X POST "http://barangkantor.bom/barang-ws" -d '{"kode": "B256ab", "nama": "ember","deskripsi":"untuk tampung air","id_kantor":"10"}'
curl -u bb:123456 -H "Content-Type:application/json" -XPUT "http://barangkantor.bom/barang-ws/43" -d '{"nama": "termos buatan","deskripsi":"termos aseli li"}'
curl -u bb:123456 -XDELETE "http://barangkantor.bom/barang-ws/43"

-------------
use bearer token
curl -i -H "Authorization: Bearer e12Pzdai1N3tp8PG214duErToNaz4zAP"  "http://barangkantor.bom/barang-ws"
curl -i -H "Authorization: Bearer e12Pzdai1N3tp8PG214duErToNaz4zAP" "Accept:application/json" "http://barangkantor.bom/barang-ws"
curl -i -H "Authorization: Bearer e12Pzdai1N3tp8PG214duErToNaz4zAP" "Accept:application/xml" -XGET "http://barangkantor.bom/barang-ws/24"
curl -i -H "Authorization: Bearer e12Pzdai1N3tp8PG214duErToNaz4zAP" "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://barangkantor.bom/barang-ws" -d '{"kode": "B27a", "nama": "henfon","deskripsi":"Henfon abad 21 yg bagus","id_kantor":"10"}'
curl -i -H "Authorization: Bearer e12Pzdai1N3tp8PG214duErToNaz4zAP" "Accept:application/xml" -XDELETE "http://barangkantor.bom/barang-ws/26"
curl -i -H "Authorization: Bearer e12Pzdai1N3tp8PG214duErToNaz4zAP" "Accept:application/json" -H "Content-Type:application/json" -XPUT "http://barangkantor.bom/barang-ws/27" -d '{"nama": "termos terharu","deskripsi":"barang bagus dan awet"}'
curl -i -H "Authorization: Bearer e12Pzdai1N3tp8PG214duErToNaz4zAP" "http://barangkantor.bom/barang-ws/lihat?nama=ter"


no need token
curl "http://localhost/biasa/web/barang-ws/lihat?nama=ter"
//throw message need token
curl "http://localhost/biasa/web/barang-ws"
 */
namespace app\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use app\models\Barang;
use app\models\User;

class BarangWsController extends ActiveController
{
	public $modelClass = 'app\models\Barang';
	
	public function init()
	{
		parent::init();
		\Yii::$app->user->enableSession = false;
	}
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
		          
				'class' => HttpBasicAuth::className(),
    		    'auth' => function ($username, $password) {
                      $user = User::find()->where(['username' => $username])->one();
                      if ($user->validatePassword($password)) {
                          return $user;
                      }
                      return null;
                  },
                  
		        //'class' => HttpBearerAuth::className(),
				'only' => ['index', 'view','create', 'update', 'delete'],
		];
		
		return $behaviors;
	}
	
	/*
    public function actionIndex()
    {
        return $this->render('index');
    }*/

    //kustom, untuk semua tanpa token
    //http://localhost/biasa/web/barang-ws/lihat?nama=ter
    public function actionLihat($nama)
    {
    	$result = Barang::find()
    	->where(['like', 'nama', $nama])
    	->all();
    
    	return $result;
    }
}
